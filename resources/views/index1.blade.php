
@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,700,900&display=swap"
          rel="stylesheet"
    />
    <title>TODO-list with JS</title>
    <link rel="stylesheet" href="{{asset('css/main.css') }}" />
    <script defer src="{{asset('js/script.js') }}"></script>
</head>
<body>
<h1 class="title">Mini  list</h1>

<div class="all-tasks" search-try>
    <h2 class="task-list-title">Tag's Name</h2>
    <ul class="task-list" data-lists></ul>
    <form action="" data-new-list-form>
        <input type="text" class="new list" data-new-list-input
               placeholder="New list" aria-label="new list name">
        <button class="btn create" aria-label="create a new list">+</button>
    </form>
</div>
<div class="todo-list" data-list-display-container>
    <div class="todo-header">
        <h2 class="list-title" data-list-title></h2>
        <p class="task-count" data-list-count> </p>
    </div>

    <div class="todo-body">
        <div class="tasks" data-tasks></div>

        <div class="new-task-creator">
            <form action="" data-new-task-form>
                <input type="text" class="new task" data-new-task-input placeholder="Task name" aria-label="task name">
                <button class="btn create" aria-label="create a new task">+</button>
            </form>

        </div>


    </div>
    <div class="delete-stuff">
        <button class="btn delete" data-delete-list-button>Delete a tag name</button>
        <button class="btn delete " data-clear-complete-tasks-button> Delete task</button>
    </div>

</div>
<template id="task-template">
    <div class="task">
        <input type="checkbox"/>
        <label>
            <span class="custom-checkbox"></span>
        </label>
    </div>
</template>

<div>
    <form id="search-list">
        <span>Tag finder</span>
        <input type="text" class="searchbar" placeholder="search a tag">
    </form>
    <p id="demo"></p>

</div>
</body>
</html>
@endsection
